/* etape 1:  creer utilisateur labcode_user*/
/*
CREATE USER `labcode_user`@`localhost` IDENTIFIED BY `123456`;
GRANT ALL PRIVILEGES ON * . * TO `labcode_user`@`localhost`;
FLUSH PRIVILEGES;
*/


/* etape 2: creer une base de donnees labcode_db*/
/*
CREATE DATABASE IF NOT EXISTS labcode_db SET `utf8`;
USE labcode_db;
*/


/** etape 3: creer des tables et importer des donnees
 mysql -u labcode_user -p;
 use labcode_db;
 source /chemin.../LCR_database.sql
 * 
 * */
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `credentials_expired` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `account_enabled` bit(1) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_hint` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
   `nickname_user` varchar(20) NOT NULL,
   `secret_question` varchar(50) NOT NULL,
   `secret_response` varchar(50) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ;

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;
INSERT INTO `app_user` VALUES (-3,'\0','\0','','Denver','US','80210','CO','\0','two_roles_user@appfuse.org','','Two Roles','User','$2a$10$bH/ssqW8OhkTlIso9/yakubYODUOmh.6m5HEJvcBq3t3VdBh7ebqO','Not a female kitty.','','two_roles_user', '' , '', '', 1,'http://raibledesigns.com'),
(-2,'\0','\0','','Denver','US','80210','CO','\0','matt@raibledesigns.com','','Matt','Raible','$2a$10$bH/ssqW8OhkTlIso9/yakubYODUOmh.6m5HEJvcBq3t3VdBh7ebqO','Not a female kitty.','','admin', '' , '', '', 1,'http://raibledesigns.com'),
(-1,'\0','\0','','Denver','US','80210','CO','\0','matt_raible@yahoo.com','','Tomcat','User','$2a$10$CnQVJ9bsWBjMpeSKrrdDEeuIptZxXrwtI6CZ/OgtNxhIgpKxXeT9y','A male kitty.','','user', '' , '', '',1,'http://tomcat.apache.org');
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (-2,'Default role for all Users','ROLE_USER'),(-1,'Administrator role (can edit Users)','ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`), 
  FOREIGN KEY (`user_id`) REFERENCES `app_user` (`id`),
  FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
); 

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (-3,-2),(-1,-2),(-3,-1),(-2,-1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
	`id_project` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`project_name` VARCHAR(20) NOT NULL,
	`project_description` TEXT NOT NULL,
	`id_author` INT UNSIGNED NOT NULL,
	`deposit_date` DATE NOT NULL DEFAULT '0000-00-00',
	`project_version` INT UNSIGNED,
	`project_key_words` VARCHAR(30) NOT NULL,
	`project_right` VARCHAR(10) NOT NULL,
	`number_views` INT UNSIGNED,
	`number_downloads` INT UNSIGNED,
	PRIMARY KEY (`id_project`),
	FOREIGN KEY (`id_author`) REFERENCES `user_account`(`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `repository`;

CREATE TABLE `repository` (
	`id_repository` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_project` INT UNSIGNED NOT NULL,
	`id_parent_repository` INT UNSIGNED,
	PRIMARY KEY (`id_repository`),
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_parent_repository`) REFERENCES `repository`(`id_repository`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
	`id_file` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`file_type` VARCHAR(20) NOT NULL,
	`file_content` MEDIUMBLOB NOT NULL,
	`file_extension` VARCHAR(20) NOT NULL,
	`id_repository` INT UNSIGNED,
	`id_project` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id_file`),
	FOREIGN KEY (`id_repository`) REFERENCES `repository`(`id_repository`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `comment_file`;

CREATE TABLE `comment_file` (
	`id_comment` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_user` INT UNSIGNED NOT NULL,
	`id_file` INT UNSIGNED NOT NULL,
	`comment_content` TEXT NOT NULL,
	PRIMARY KEY (id_comment,id_user,id_file),
	FOREIGN KEY (id_user) REFERENCES user_account(id_user) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_file`) REFERENCES `file`(`id_file`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `comment_project`;

CREATE TABLE `comment_project` (
	`id_comment` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`id_user` INT UNSIGNED NOT NULL,
	`id_project` INT UNSIGNED NOT NULL,
	`comment_content` TEXT NOT NULL,
	PRIMARY KEY (`id_comment`,`id_user`,`id_project`),
	FOREIGN KEY (`id_user`) REFERENCES `user_account`(`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`id_project`) REFERENCES `project`(`id_project`) ON DELETE CASCADE ON UPDATE CASCADE
);
