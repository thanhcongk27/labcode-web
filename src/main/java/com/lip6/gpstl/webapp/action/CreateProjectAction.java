package com.lip6.gpstl.webapp.action;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CreateProjectAction extends BaseAction {
	
	/**
	 * 
	 */
	ProjectDao projectDao;
	
	private static final long serialVersionUID = 1L;
	
	public String execute() {
		return SUCCESS;
	}
	
	public String create() {
		if (getRequest().getMethod().equalsIgnoreCase("post")) {
			if(getRequest().getAttribute("project_name") != null && getRequest().getAttribute("id_author") != null && getRequest().getAttribute("project_description")!= null 
					&& getRequest().getAttribute("keywords")!= null && getRequest().getAttribute("project_right")!= null){
				String projectName = (String) getRequest().getAttribute("project_name");
				String idAuthor = (String) getRequest().getAttribute("id_author");
				String projectDescription = (String) getRequest().getAttribute("project_description");
				String keywords = (String) getRequest().getAttribute("project_key_words");
				String projectRight = (String) getRequest().getAttribute("project_right");
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date currentDate = new Date();
				Project project = new Project(projectName,projectDescription,idAuthor,currentDate,keywords,projectRight);
				
				projectDao.createProject(project);
				return SUCCESS;
			}
		}
		
		getSession().setAttribute("errors", "Remplire tout les champs.");
		return INPUT;
	}
	
}
