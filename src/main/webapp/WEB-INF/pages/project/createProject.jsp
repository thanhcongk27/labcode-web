<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="home.title"/></title>
    <meta name="menu" content="Home"/>
</head>
<body class="home">

<h2><fmt:message key="home.heading"/></h2>
<h3>Cr�er un projet</h3>
<!-- 
<p><fmt:message key="home.message"/></p>
 
<s:form action="validateCreateProject" enctype="multipart/form-data" method="post" validate="true" id="uploadForm" cssClass="well">
        
        <div id="actions" class="form-group">
            <s:submit type="button" key="button.done" cssClass="btn btn-primary" theme="simple" method="create">
                <i class="icon-upload icon-white"></i>
                <fmt:message key="button.done"/>
            </s:submit>

            <a class="btn btn-default" href="home" >
                <i class="icon-remove"></i>
                <fmt:message key="button.cancel"/>
            </a>
        </div>
    </s:form> -->
<div> 
	<s:form name="CreateProjectForm" action="" method="POST" cssClass="well">
		Nom:*
	    <s:textfield  name="project_name" required="true" cssClass="form-control"/>
		Description:*
		<s:textarea name="project_description" required="true" cssClass="form-control"/>
		Mot cl�:*
		<s:textfield name="project_key_words" required="true" cssClass="form-control"/>
		Droit d'acc�s:*
		<s:radio list="{'Public','Priv�'}" name="project_right" required="true"></s:radio>
		<s:submit  value="Valider" name="btnCreateProject" cssClass="btn btn-primary"></s:submit>	
	</s:form>
</div>	
</body>